/**
 * Created by antness on 9/10/14.
 */
var demo = angular.module('demo', ['nnMux']);

demo.config(function(nnMuxProvider) {
    var actions = {
        method1: 1,
        namespace: ['method1']
    };
    nnMuxProvider.register('myBestAPI',
        actions,
        {
            url: '/api',
            throttle: 5000
        }
    );
});

demo.controller('nnMuxDemoCtrl', function($scope, myBestAPI) {
    $scope.model = {
        frequency: 0
    };

    var interval = null;
    function callAction() {
        myBestAPI.namespace.method1().then(console.log, console.error);
    }

    $scope.$watch('model.frequency', function(frequency) {
        if(interval !== null)
            clearInterval(interval);
        if(frequency > 0)
            interval = setInterval(callAction, Math.floor(1/frequency*1000))
    });
});